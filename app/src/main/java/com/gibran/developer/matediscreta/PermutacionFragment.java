package com.gibran.developer.matediscreta;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gibran.developer.matediscreta.util.Functions;

import org.w3c.dom.Text;


/**
 * A simple {@link Fragment} subclass.
 */
public class PermutacionFragment extends Fragment {

    private TextView tvN, tvN2;
    private TextView tvR;
    private Button btnInfo;
    private EditText etResult, etValue;

    private View viewAlertInput;

    public PermutacionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_permutacion, container, false);

        this.tvN = (TextView)view.findViewById(R.id.tvN);
        this.tvN2 = (TextView)view.findViewById(R.id.tvN2);
        this.tvR = (TextView)view.findViewById(R.id.tvR);
        this.btnInfo = (Button)view.findViewById(R.id.btnInfo);
        this.etResult = (EditText)view.findViewById(R.id.etResult);


        this.tvN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewAlertInput = inflater.inflate(R.layout.dialog_input_text, null);
                AlertDialog.Builder builderDialog = new AlertDialog.Builder(getActivity());
                builderDialog.setView(viewAlertInput);
                builderDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        etValue = (EditText)viewAlertInput.findViewById(R.id.etValue);
                        tvN.setText(etValue.getText());
                        tvN2.setText(etValue.getText());

                        int total = 0;
                        try{
                            total = Functions.permutacion(Integer.parseInt(tvN.getText().toString()), Integer.parseInt(tvR.getText().toString()));
                        }catch(NumberFormatException e){
                            e.getMessage();
                        }
                        etResult.setText(String.valueOf(total));

                    }
                }).create().show();
            }
        });
        this.tvR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewAlertInput = inflater.inflate(R.layout.dialog_input_text, null);
                AlertDialog.Builder builderDialog = new AlertDialog.Builder(getActivity());
                builderDialog.setView(viewAlertInput);
                builderDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        etValue = (EditText)viewAlertInput.findViewById(R.id.etValue);
                        tvR.setText(etValue.getText());

                        int total = 0;
                        try{
                            total = Functions.permutacion(Integer.parseInt(tvN.getText().toString()), Integer.parseInt(tvR.getText().toString()));
                        }catch(NumberFormatException e){
                            e.getMessage();
                        }catch(ArithmeticException e){
                            e.getMessage();
                        }
                        etResult.setText(String.valueOf(total));
                    }
                }).create().show();
            }
        });

        this.btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder infoDialog = new AlertDialog.Builder(getActivity());
                infoDialog.setTitle("Information");
                infoDialog.setMessage("Una permutación de objetos es un arreglo de éstos en el que orden sí importa.  Para encontrar el número de permutaciones de n objetos diferentes en grupos de r.");
                infoDialog.create().show();
            }
        });

        return view;
    }

}
