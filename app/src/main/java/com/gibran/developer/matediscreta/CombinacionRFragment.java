package com.gibran.developer.matediscreta;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gibran.developer.matediscreta.util.Functions;


/**
 * A simple {@link Fragment} subclass.
 */
public class CombinacionRFragment extends Fragment {

    private TextView tvN, tvN2;
    private TextView tvR, tvR2;
    private EditText etTotal, etValue;
    private Button btnInfo;

    private View viewAlertInput;

    public CombinacionRFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_combinacion_r, container, false);
        this.tvN = (TextView)view.findViewById(R.id.tvN);
        this.tvN2 = (TextView)view.findViewById(R.id.tvN2);
        this.tvR = (TextView)view.findViewById(R.id.tvR);
        this.tvR2 = (TextView)view.findViewById(R.id.tvR2);

        this.etTotal = (EditText)view.findViewById(R.id.etTotal);

        this.tvN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewAlertInput = inflater.inflate(R.layout.dialog_input_text, null);
                AlertDialog.Builder builderDialog = new AlertDialog.Builder(getActivity());
                builderDialog.setView(viewAlertInput);
                builderDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        etValue = (EditText)viewAlertInput.findViewById(R.id.etValue);
                        if(etValue.getText().toString().length() >= 2){
                            tvN.setTextSize(20);
                            tvN2.setTextSize(20);
                        }else{
                            tvN.setTextSize(34);
                            tvN2.setTextSize(34);
                        }
                        if(etValue.getText().toString().length() >= 4){
                            tvN.setTextSize(18);
                            tvN2.setTextSize(18);
                        }
                        tvN.setText(etValue.getText().toString());
                        tvN2.setText(etValue.getText().toString());

                        int total = 0;
                        try{
                            total = Functions.combinacionRepeticion(Integer.parseInt(tvN.getText().toString()), Integer.parseInt(tvR.getText().toString()));
                        }catch(NumberFormatException e){
                            e.getMessage();
                        }catch(ArithmeticException e){
                            e.getMessage();
                        }
                        etTotal.setText(String.valueOf(total));
                    }
                });
                builderDialog.create().show();
            }
        });


        this.tvR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewAlertInput = inflater.inflate(R.layout.dialog_input_text, null);
                AlertDialog.Builder builderDialog = new AlertDialog.Builder(getActivity());
                builderDialog.setView(viewAlertInput);
                builderDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        etValue = (EditText) viewAlertInput.findViewById(R.id.etValue);
                        if (etValue.getText().toString().length() >= 2) {
                            tvR.setTextSize(20);
                            tvR2.setTextSize(20);
                        } else {
                            tvR.setTextSize(34);
                            tvR2.setTextSize(34);
                        }
                        if (etValue.getText().toString().length() >= 4) {
                            tvR.setTextSize(18);
                            tvR2.setTextSize(18);
                        }
                        tvR.setText(etValue.getText().toString());
                        tvR2.setText(etValue.getText().toString());

                        int total = 0;
                        try{
                            total = Functions.combinacionRepeticion(Integer.parseInt(tvN.getText().toString()), Integer.parseInt(tvR.getText().toString()));
                        }catch(NumberFormatException e){
                            e.getMessage();
                        }
                        etTotal.setText(String.valueOf(total));
                    }
                });
                builderDialog.create().show();
            }
        });



        this.btnInfo = (Button)view.findViewById(R.id.btnInfo);
        this.btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder infoDialog = new AlertDialog.Builder(getActivity());
                infoDialog.setTitle("Information");
                infoDialog.setMessage("Llamamos combinación con repetición de m elementos de A a todo subconjunto de m elementos de A en el que un elemento puede aparecer hasta m veces. En este caso sólo nos importa la naturaleza, no el orden y además podemos repetir elementos.");
                infoDialog.create().show();
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

}
