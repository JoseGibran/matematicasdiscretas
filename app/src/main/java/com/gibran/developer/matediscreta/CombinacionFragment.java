package com.gibran.developer.matediscreta;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gibran.developer.matediscreta.util.Functions;


public class CombinacionFragment extends Fragment{

    private TextView tvN, tvN2, tvN3;
    private TextView tvK, tvK2, tvK3;
    private EditText etTotal;

    private Button btnInfo;

    private EditText etValue;
    private AlertDialog.Builder builderDialog;


    private View viewAlertInput;

    public CombinacionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_combinacion, container, false);
        this.tvN = (TextView)view.findViewById(R.id.tvR);
        this.tvN2 = (TextView)view.findViewById(R.id.tvN2);
        this.tvN3 = (TextView)view.findViewById(R.id.tvN3);
        this.tvK = (TextView)view.findViewById(R.id.tvK);
        this.tvK2 = (TextView)view.findViewById(R.id.tvK2);
        this.tvK3 = (TextView)view.findViewById(R.id.tvK3);

        this.btnInfo = (Button)view.findViewById(R.id.btnInfo);
        this.btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder infoDialog = new AlertDialog.Builder(getActivity());
                infoDialog.setTitle("Information");
                infoDialog.setMessage("Se llama combinaciones de m elementos tomados de n en n (m ≥ n) a todas las agrupaciones posibles que pueden hacerse con los m elementos de forma que:\n" +
                        "No entran todos los elementos.\n" +
                        "No importa el orden.\n" +
                        "No se repiten los elementos.\n");
                infoDialog.create().show();
            }
        });


        this.etTotal  = (EditText)view.findViewById(R.id.etTotal);

        this.tvN.setOnTouchListener(new View.OnTouchListener() {
           @Override
           public boolean onTouch(View v, MotionEvent event) {

               viewAlertInput = inflater.inflate(R.layout.dialog_input_text, null);
               builderDialog = new AlertDialog.Builder(getActivity());
               builderDialog.setView(viewAlertInput);
               AlertDialog alert = getAlertInputValue(new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialog, int which) {
                       etValue = (EditText)viewAlertInput.findViewById(R.id.etValue);

                       if(etValue.getText().toString().length() >= 2){
                           tvN2.setTextSize(20);
                           tvN3.setTextSize(20);
                       }else{
                           tvN2.setTextSize(34);
                           tvN3.setTextSize(34);
                       }
                       if(etValue.getText().toString().length() >= 4){
                           tvN2.setTextSize(18);
                           tvN3.setTextSize(18);
                       }
                       tvN2.setText(etValue.getText().toString());
                       tvN3.setText(etValue.getText().toString());

                       long total = 0;
                       try{
                           total = Functions.combinacion(Integer.parseInt(tvN2.getText().toString()), Integer.parseInt(tvK2.getText().toString()));
                       }catch(NumberFormatException e){
                           etTotal.setText(e.getMessage());
                       }
                       etTotal.setText(String.valueOf(total));

                   }
               });
               alert.show();
               return false;
           }
        });
        this.tvK.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                viewAlertInput = inflater.inflate(R.layout.dialog_input_text, null);
                builderDialog = new AlertDialog.Builder(getActivity());
                builderDialog.setView(viewAlertInput);
                AlertDialog alert = getAlertInputValue(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        etValue = (EditText)viewAlertInput.findViewById(R.id.etValue);

                        if(etValue.getText().toString().length() >= 3){
                            tvK2.setTextSize(20);
                            tvK3.setTextSize(20);
                        }else{
                            tvK2.setTextSize(34);
                            tvK3.setTextSize(34);
                        }
                        if(etValue.getText().toString().length() >= 5){
                            tvK2.setTextSize(18);
                            tvK3.setTextSize(18);
                        }


                        tvK2.setText(etValue.getText().toString());
                        tvK3.setText(etValue.getText().toString());
                        long total = 0;
                        try {
                            total = Functions.combinacion(Integer.parseInt(tvN2.getText().toString()),  Integer.parseInt(tvK2.getText().toString()));
                            etTotal.setText(String.valueOf(total));
                        }catch(NumberFormatException e){
                            etTotal.setText(e.getMessage());
                        }

                    }
                });
                alert.show();

                return false;
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    public AlertDialog getAlertInputValue(DialogInterface.OnClickListener callback){

        this.builderDialog.setPositiveButton("Ok", callback);
        return this.builderDialog.create();
    }

}

