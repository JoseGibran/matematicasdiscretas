package com.gibran.developer.matediscreta.util;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Usuario on 03/10/2016.
 */
public class Functions {


    public static int permutacion(int num, int r){
        int i,resp, resp2, total ;
        r = num-r;
        resp = 1;
        for(i=1;i<=num;i++){
            resp = resp * i;
        }
        resp2 = 1;
        for(i=1;i<=r;i++){
            resp2 = resp2 * i;
        }
        total = resp/resp2;
        return total;
    }

    public static long combinacion(int n, int r){
        long i,resp, resp2, resp3, total;
        resp = 1;
        for(i=1;i<=n;i++){
            resp = resp * i;
        }
        resp2 = 1;
        for(i=1;i<=r;i++){
            resp2 = resp2 * i;
        }
        r= n-r;
        resp3= 1;
        for(i=1;i<=r;i++){
            resp3 = resp3 * i;
        }
        total = resp/ (resp2 * resp3);
        return total;
    }
    public static int combinacionRepeticion(int n, int r){
        int i, num, resp, resp2, resp3, total;
        num = (n + r -1);
        n = (n-1);
        resp = 1;
        for(i=1;i<=num;i++){
            resp = resp * i;
        }
        resp2 = 1;
        for(i=1;i<=r;i++){
            resp2 = resp2 * i;
        }
        r= n-r;
        resp3= 1;
        for(i=1;i<=n;i++){
            resp3 = resp3 * i;
        }
        total = resp/ (resp2 * resp3);
        return total;
    }

    public static String teorema(int n, String a, String b){
        String result  = "";
        for(int i = 0; i<= n; i++){
            String coefficient = "";
            String expA = "";
            String expB = "";
            String plus= "";

            String termA = "";
            String termB = "";

            if((n - i)!= 1){
                expA = "<sup>" + String.valueOf(n - i) + "</sup>";
            }
            if( i != 1){
                expB = "<sup>" + String.valueOf(i) + "</sup>" ;
            }
            if(i != n){
                plus = " + ";
            }
            if(combinacion(n, i) != 1){
                coefficient = String.valueOf(combinacion(n, i));
            }
            if((n - i) > 0){
                termA = a + expA;
            }
            if(i > 0){
                termB = b + expB;
            }

            result +=  coefficient + termA +termB + plus;
        }
        result =
                "<html><head>"
                        + "<style type=\"text/css\">body{color: #fff}"
                        + "</style></head>"
                        + "<body>"
                        + "<center>"
                        + "<h2>"
                        + result
                        + "</h2>"
                        + "</center>"
                        + "</body></html>";
        return result;
    }


    public static List<int[]> convinate(int[] numbers){
        List<int[]> convinations = new ArrayList<int[]>();
        for(int i = 0; i < numbers.length; i ++){
            for(int j = i + 1; j < numbers.length; j++){
                int convination[] = {numbers[i], numbers[j]};
                convinations.add(convination);
            }
        }
        return convinations;
    }
    public static List<int[]> patron(int[] numbers){
        List<int[]> patron = new ArrayList<int[]>();
        List<Integer> selectedNumbers = new ArrayList<Integer>();

        int a = getRandom(0, numbers.length - 1);
        int b = getRandom(0, numbers.length - 1);

        selectedNumbers.add(a);
        selectedNumbers.add(b);

        int flag = getRandom(2, numbers.length -3);

        for(int i = 0; i< numbers.length - flag ; i++){

            int stage[] = {numbers[a], numbers[b]};
            patron.add(stage);
            a = b;

            while(isUsed(selectedNumbers, b)){
                b = getRandom(0, numbers.length - 1);
            }
            selectedNumbers.add(b);

        }
        return patron;
    }

    public static boolean isUsed(List<Integer> list, int raw){
        boolean result = false;

        for(int i = 0; i < list.size(); i++){
            if(list.get(i) == raw){
                result = true;
            }
        }
        return result;
    }

    public static  int getRandom(int aStart, int aEnd){
        Random random = new Random();
        if (aStart > aEnd) {
            throw new IllegalArgumentException("Start cannot exceed End.");
        }
        long range = (long)aEnd - (long)aStart + 1;
        long fraction = (long)(range * random.nextDouble());
        int randomNumber =  (int)(fraction + aStart);
        return randomNumber;
    }
}
