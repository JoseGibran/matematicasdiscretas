package com.gibran.developer.matediscreta;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.gibran.developer.matediscreta.util.Functions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;


/**
 * A simple {@link Fragment} subclass.
 */
public class GrafosFragment extends Fragment {

    private List<Point> pointCircles = new ArrayList<Point>();
    private Canvas mCanvas;
    private Button btnReload;

    private int[] teams = {1,2,4,5,6,7,8,9, 10};
    private List<int[]> matchs = Functions.patron(teams);
    private HashMap<Integer, Point> matchPositions = new HashMap<Integer, Point>();

    public GrafosFragment() {
        // Required empty public constructor
    }


    public class Lienzo extends View implements View.OnTouchListener {

        //constructor
        private int minX, maxX, minY,maxY;
        private int countX, countY;
        public Lienzo(Context context) {
            super(context);
            mCanvas =new Canvas();
            setOnTouchListener(this);

            btnReload.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    matchs = Functions.patron(teams);
                    renderTeams();
                    invalidate();
                }
            });
        }

        public void renderTeams(){
            pointCircles.clear();
            countX = minX  + 40;
            countY = minY  + 40;
            int localCount = 0;
            for(int team : teams){
                Point p = new Point();

                if(localCount > 2){
                    countY += (maxY / 2) - 80 ;
                    countX = minX + 40;
                    localCount = 0;
                }

                p.x = countX;
                p.y =  countY;
                pointCircles.add(p);
                matchPositions.put(team, p);
                countX +=  (maxX / 2) - 80;
                localCount ++;
            }
        }
        @Override
        protected void onDraw(Canvas canvas) {
            // TODO Auto-generated method stub
            super.onDraw(canvas);
            mCanvas =  canvas;
            Paint linePaint = new Paint();
            linePaint.setColor(Color.rgb(117, 117, 117));
            linePaint.setStrokeWidth(20);



            for(int i = 0; i< matchs.size() ; i++){
                Point start = matchPositions.get(matchs.get(i)[0]);
                Point end = matchPositions.get(matchs.get(i)[1]);
                mCanvas.drawLine(start.x, start.y, end.x, end.y, linePaint);
            }
           /* for(int i = 0; i< pointCircles.size(); i++){
                if(i != 0){
                    mCanvas.drawLine(pointCircles.get(i - 1 ).x , pointCircles.get(i - 1).y, pointCircles.get(i).x, pointCircles.get(i).y , linePaint);
                }
            }*/
            for(int i = 0; i < pointCircles.size(); i++){
                addCircle(String.valueOf(i),pointCircles.get(i).x, pointCircles.get(i).y, mCanvas);
            }
            mCanvas.drawText( "WIDTH: min= " + this.minX + " max= " + this.maxX, 10, 10, linePaint);
            mCanvas.drawText("HEIGHT: min= " + this.minY + " max= " + this.maxY, 10, 30, linePaint);
        }
        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            this.minX = 50;
            this.maxX = w - 50;
            this.minY = 50;
            this.maxY = h - 50;
            renderTeams();
            super.onSizeChanged(w, h, oldw, oldh);
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch(event.getAction()){
                case MotionEvent.ACTION_DOWN:
                    Point p = new Point();
                    p.x = (int) event.getX();
                    p.y = (int) event.getY();
                    //pointCircles.add(p);
                        //Toast.makeText(getContext(), "Works fine", Toast.LENGTH_SHORT).show();
                    invalidate();
                    return true;
            }
            return false;
        }
        private void addCircle(String text, float x, float y, Canvas canvas){
            Paint pincel= new Paint();
            pincel.setStyle(Paint.Style.FILL); //STROKE
            //pincel.setStrokeWidth(20);
            pincel.setTextSize(30);
            pincel.setTypeface(Typeface.MONOSPACE);
            pincel.setColor(Color.parseColor("#03A9F4"));
            Paint paint = new Paint();
            paint.setColor(Color.WHITE);
            paint.setTextSize(40);
            canvas.drawCircle(x, y, 100, pincel);
            canvas.drawText(text, x - 10, y + 10, paint);
        }
        private  int getRandom(int aStart, int aEnd){
            Random random = new Random();
            if (aStart > aEnd) {
                throw new IllegalArgumentException("Start cannot exceed End.");
            }
            long range = (long)aEnd - (long)aStart + 1;
            long fraction = (long)(range * random.nextDouble());
            int randomNumber =  (int)(fraction + aStart);
            return randomNumber;
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_grafos, container, false);
        container = (RelativeLayout)view.findViewById(R.id.container);

        btnReload = (Button)view.findViewById(R.id.btnReload);
        container.addView(new Lienzo(getActivity()));
        return view;
    }

    private boolean inCircle(float x, float y, float circleCenterX, float circleCenterY, float circleRadius) {
        double dx = Math.pow(x - circleCenterX, 2);
        double dy = Math.pow(y - circleCenterY, 2);
        if ((dx + dy) < Math.pow(circleRadius, 2)) {
            return true;
        } else {
            return false;
        }
    }

}
