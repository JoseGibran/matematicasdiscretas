package com.gibran.developer.matediscreta;


import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gibran.developer.matediscreta.util.Functions;


/**
 * A simple {@link Fragment} subclass.
 */
public class TeoremaFragment extends Fragment {

    private TextView tvN, tvN2, tvN3, tvN4;
    private EditText  etValue;
    private Button btnInfo;

    private WebView wvResult;

    private View viewAlertInput;

    public TeoremaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_teorema, container, false);
        this.tvN = (TextView)view.findViewById(R.id.tvN);
        this.tvN2 = (TextView)view.findViewById(R.id.tvN2);
        this.tvN3 = (TextView)view.findViewById(R.id.tvN3);
        this.tvN4 = (TextView)view.findViewById(R.id.tvN4);
        this.btnInfo = (Button)view.findViewById(R.id.btnInfo);

        this.wvResult = (WebView)view.findViewById(R.id.wvResult);

        wvResult.getSettings();
        wvResult.setBackgroundColor(Color.rgb(3,169,244));




        this.tvN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewAlertInput = inflater.inflate(R.layout.dialog_input_text, null);
                AlertDialog.Builder builderDialog = new AlertDialog.Builder(getActivity());
                builderDialog.setView(viewAlertInput);
                builderDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        etValue = (EditText) viewAlertInput.findViewById(R.id.etValue);

                        tvN.setText(etValue.getText());
                        tvN2.setText(etValue.getText());
                        tvN3.setText(etValue.getText());
                        tvN4.setText(etValue.getText());
                        wvResult.loadData(Functions.teorema(Integer.parseInt(etValue.getText().toString()), "a", "b"), "text/html; charset=UTF-8", null);
                    }
                });
                builderDialog.create().show();
            }
        });
        this.btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder infoDialog = new AlertDialog.Builder(getActivity());
                infoDialog.setTitle("Information");
                infoDialog.setMessage("El teorema del binomio es una fórmula (por esto se llama también fórmula del binomio) con la cual se puede escribir directamente los términos del desarrollo de una potencia entera y positiva de un binomio.");
                infoDialog.create().show();
            }
        });
        return view;
    }
}
